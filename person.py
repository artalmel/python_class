#!/usr/bin/python3

class Person:
    "this class describes the man"
    personCount = 0 #the number of people

    def __init__(self, name, age, work, phoneNumber):
        self.name = name
        self.age = age
        self.work = work
        self.phone='(%.3s)' % phoneNumber + phoneNumber[3:len(phoneNumber)]
        Person.personCount += 1



    #checking on what kind of connection between the people
    def ifFriend(self,name,age,work):
        if int(self.age) >= age-5 and int(self.age) <= age+5 and self.work == work:
            print('%s and %s are friends and colleague' % (self.name, name))
        elif int(self.age) >= age-5 and int(self.age) <= age+5:
            print("{0} and {1} are friends ".format(self.name,name))
        elif self.work == work:
            print("{0} and {1} are colleague".format(self.name,name))
        else:
            print('%s and %s have nothing to do with each other' % (self.name,name))


    #Information about people
    def displayPerson(self):
        print('Name:%s | Age:%s | Work:%s | Phone number: %s' % (self.name, self.age, self.work, self.phone))

#create a Person object
person1 = Person("Valod", 35, "driver",'093157614')
person2 = Person("Gugo", 58, "teacher",'093225566')
person3 = Person("Vzgo", 41, "driver",'099002930')
person4 = Person("Zaven", 32, "aaa",'094522771')

#create an array of Person
array=[person1,person2,person3,person4]

for i in array:
    i.displayPerson()

#check connections to the people
for i in range(len(array)):
    for j in range(i+1,len(array)):
        array[i].ifFriend(array[j].name,array[j].age,array[j].work)
