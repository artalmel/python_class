#!/usr/bin/python3
class stack:
    def __init__(self):
        self.array=[]

    #add value to stack
    def push(self):
        while True:
            arg=input('input argument:')
            if arg is '':
               return False
            else:
                self.array.insert(0,arg)

    #return value stack
    def pull(self):
        try:
            element=self.array[-1]
            del(self.array[-1])
            print(element)
            return element
        except:
            print('stack empty')

    #check out the array is empty
    def isEmpty(self):
        if len(self.array)==0:
            print('stack is empty')
        else:
            print('stack length=%s' % len(self.array))

    #compare the arrays
    def compareArrays(self,arg):
        for i in range(len(self.array)):
            if self.array[i]!=arg[i]:
                self.array=arg
        print(self.array)

